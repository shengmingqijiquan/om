package com.zero2oneit.mall.system.common.handle;

import com.zero2oneit.mall.common.exception.CustomException;
import com.zero2oneit.mall.common.exception.InvalidateSessionsException;
import com.zero2oneit.mall.common.utils.R;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 全局异常处理器
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private ObjectError error;

    /**
     * 处理自定义的异常
     */
    @ExceptionHandler(CustomException.class)
    @ResponseBody
    public R customExceptionHandler(CustomException e){
        return R.fail(e.getMessage());
    }

    /**
     * 处理权限自定义的异常
     */
    @ExceptionHandler(AuthenticationException.class)
    @ResponseBody
    public R authenticationExceptionHandler(AuthenticationException e){
        return R.fail(e.getMessage());
    }

    /**
     * 回话过期异常
     */
    @ExceptionHandler(InvalidateSessionsException.class)
    @ResponseBody
    public R invalidateSessionsExceptionExceptionHandler(InvalidateSessionsException e){
        return R.fail("gq");
    }

    /**
     * 处理bean validtation校验异常
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    public R exceptionHandler(BindException e){
        StringBuilder sb = new StringBuilder() ;
        List<ObjectError> errors = e.getAllErrors();
        for(ObjectError error : errors){
            sb.append(error.getDefaultMessage()).append(" ; ") ;
        }
        return R.fail(sb.toString());
    }

}
