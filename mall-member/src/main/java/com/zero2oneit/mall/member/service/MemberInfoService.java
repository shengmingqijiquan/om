package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.auth.AppWxLoginObject;
import com.zero2oneit.mall.common.bean.auth.RegisterInfo;
import com.zero2oneit.mall.common.query.member.InfoQueryObject;
import com.zero2oneit.mall.common.bean.member.InfoSms;
import com.zero2oneit.mall.common.bean.member.MemberInfo;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;


import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author Sinper
 * @create 2020-07-15
 * @description
 */
public interface MemberInfoService extends IService<MemberInfo> {

    R updatePhone(InfoSms infoSms);

    Boolean sendMsg(Map<String, String> map);

    void updateRedis(MemberInfo info);

    R updatePay(InfoSms infoSms);

    void updateInfoSms(InfoSms infoSms);

    BoostrapDataGrid queryInfo(InfoQueryObject qo);

    R wxauth(AppWxLoginObject appWxLoginObject);

    List<MemberInfo> queryInfoRedis(HttpServletRequest request);

    R appCodePhone(AppWxLoginObject appWxLoginObject);

    R appPassPhone(AppWxLoginObject appWxLoginObject);

    R bindWx(AppWxLoginObject appWxLoginObject);

    Boolean checkPhone(String phone);

    MemberInfo findByPhone(String phone);

    R register(AppWxLoginObject appWxLoginObject);

    R appUpdatePass(AppWxLoginObject appWxLoginObject);

    R verifyPhone(InfoSms infoSms);

    R updatePassword(Map<String, String> memberInfo);

    R verifyPay(InfoSms infoSms);

    R verifyPayMsg(InfoSms infoSms);

    R registerHtml(RegisterInfo registerInfo);

    R appeltWxauth(AppWxLoginObject appWxLoginObject);


    R feignFindMemberInfo(String userId);

    MemberInfo FindMemberInfo(String userId);


    R bingMemberStation(Map<String, String> map);

//    R createQrCodeById(String memberId, String pic) throws Exception;

    R createQrCodeByIdNew(String memberId);

    R queryPersonData(String memberId);

    R addStationSonAccount(Map<String, String> map);

    R queryStationSonAccount(String stationId);

    R deleteStationSonAccount(String phone);

    R queryStationMasterById(String stationId);

    R addLeader(Map<String, String> map);

    R upgradeVIPs(String memberId);

    R downgradeFans(String memberId);



}

