import Vue from 'vue'
import App from './App'
import store from './store'
import base from '@/common/js/api/api.config.js'
import {http} from '@/common/js/api/api.js'

Vue.config.productionTip = false;
Vue.prototype.$store = store;
Vue.prototype.$config=base;
Vue.prototype.$http = http;
App.mpType = 'app';

const app = new Vue({
    ...App
});
app.$mount();
