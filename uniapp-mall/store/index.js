import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/**
 * context : 上下文 ---> 可以获取state中的参数
 * 如：context.state.token
 */
export default new Vuex.Store({
	state: {
	  token: 'sss'
	},
	mutations: {
	  
	},
	actions: {
		getCity(context){
			
		}
	},
	getters:{

	}
})	